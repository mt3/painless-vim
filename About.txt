# About This Book

## About the Text

*Painless Vim* is by Nathanial Ellsworth Dickson, but you can call me Nate.

It was written using an unwieldy combination of (in alphabetical order):

- [Marked](http://markedapp.com/)
- [MultiMarkdown Composer](http://multimarkdown.com/)
- [Scapple](http://www.literatureandlatte.com/scapple.php)
- [Scrivener](http://www.literatureandlatte.com/scrivener.php)
- [Ulysses](http://www.ulyssesapp.com/) 
- [vim](http://www.vim.org/) (of course)

It was written on an unlikely number of macs.

## About the cover

The cover features an illustration by [Callihan](http://www.shutterstock.com/pic.mhtml?id=94462300). Used with permission.
The typefaces used on the cover are:

- **Matchbook Serif** (*title*)
- **Aleo Light** (*subtitle*)
- **Caviar Dreams** (*by line*)

The cover was designed by Nate Dickson using [iDraw](http://www.indeeo.com/idraw/) and [Pixelmator](http://www.pixelmator.com/)
